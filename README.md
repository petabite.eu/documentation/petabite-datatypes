![Petabite](Logo_Petabite_small.jpg "Logo")


# Petabite datatypes

Description and tools for data products provided by Petabite.

The following product types are documented here:

- [Sentinel-1 Ship Sighting Products](PB_S1GRD_SHS/README.md)
- [Sentinel-2 Ship Sighting Products](PB_S22ASHS/README.md)

## Commonalities

There are a number of conventions that all products provided by Petabite adhere to. If there is a product type does not follow
one or more of the conventions it is prominently stated in the documentation.

### Product naming convention

Products are named according to the following pattern:

PB_ttt...ttt_[YYYYmmDDTHHMMSS_][gggggg_][TYPE-SPECIFICS_]VVvv.zip

where
- **ttt...ttt** is the typecode identifier with 6 to 12 characters
- **YYYYmmDDTHHMMSS** is the approximate time and date of the acquisition.
- **gggggg** is the geohash of the product footprint centre points approximate position. This is only useful for products covering small areas. For large scale products this element would not be set.
- **TYPE-SPECIFICS** is a product type specific qualifier. For derived products this may be a reference to the name of the source product.
- **VVvv** is the products version number of the product. The version number allows to identify the exact algorithm implementation used to generate the product.

### Packaging

The products are by default delivered as a .zip package. However, the packaging is not considered part of the product format specification, thus
an unpacked directory still conforms to the product specification.

- Each product package contains a single root directory that is named like the package (without the extension) and that holds all the data of the package.
- Within the root directory there is at least one file "metadata.json" with at least the basic metadata defined in this common section. There is also more product type specific metadata that is documented in the product type specific sections.
- The component files carry fully qualified names. This is to allow for a merge of product directories without the overwrite of components.

### Metadata

The file metadata.json contains metadata on the product in json format. The format was chosen for ease of parsing by applications. There may be additional metadata information in the product but this basic file provides the essential information on the product to identify the product and access its components.

The following table lists the common element.

<table>
  <tr><th>Item</th><th>Description</th></tr>
  <tr><td>components</td><td>The list of files in the package.</td></tr>
  <tr><td>copyright</td><td>A copyright statement.</td></tr>
  <tr><td>creationDate</td><td>Production time of this product.</td></tr>
  <tr><td>creator</td><td>Information on the creating organisation.</td></tr>
  <tr><td>endTime</td><td>The approximate end time of the acquisition of the data in the product. For short acquisition times less than 20 seconds, may be set equal to the start time of the acquisition (startTime).</td></tr>
  <tr><td>footprint</td><td>The footprint of the product as Well Known Text (WKT).</td></tr>
  <tr><td>processor</td><td>The name of the processor (always: s2_shipextract).</td></tr>
  <tr><td>processorRunId</td><td>A unique identifier of the processor execution run that generated this product.</td></tr>
  <tr><td>processorVersion</td><td>The software version of the processor.</td></tr>
  <tr><td>productVersion</td><td>The version of the product. The version may change with a new processor version or new input data version.</td></tr>
  <tr><td>startTime</td><td>The approximate start time of the acquisition of this product.</td></tr>
  <tr><td>typeCode</td><td>An unique type code.</td></tr>
</table>

### Python tools

Some python scripts are available to interact with the Sentinel-2 ship sighting products:
- **extract** retrieves one or more components from the product package
- **info** extracts selected information from a product package and makes it available in various formats
- **bands** retrieves band data from the package and makes it available in different formats

---
* License of documentation: [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/)
* License of tools: [MIT License](tools/LICENSE)
* Get in touch: [Imprint](https://petabite.eu/imprint)
