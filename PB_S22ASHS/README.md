![Ships](ship_topic.jpg "Ships")

# Sentinel2 Ship Sighting Product

Description and tools for Sentinel-2 ship sighting products.

## Introduction

The Sentinel-2 ship sighting product contains information on ships that have been captured by the Sentinel-2 sensors. With 10 m resolution on a number of bands, the Sentinel-2 data is sufficient to identify ships and even derive approximate information on their size and orientation. Small boats can also be detected through their wake.

This ship sighting bundle product provides 
- Sentinel-2 L2A (BOA + SCL) band data around a ship position
- A pixel list of pixels with a high probability of representing a ship
- Ship position and track data

The product is intended to be used for the training of statistical and deep learning models for the detection of ships in Sentinel-2 data. A large number of products can be provided both from past Sentinel-2 data and from recent Sentinel-2 products as they are made available by Copernicus.

## Product and product component naming

The product is named according to the following pattern:

PB_S22ASHS_YYYYmmDDTHHMMSS_gggggg_iiiiiiiii_VVvv.zip

where
- **PB_** is a prefix 
- **S22ASHS_** is the fixed typecode identifer
- **YYYYmmDDTHHMMSS** is the approximate time and date of the acquisition
- **gggggg** is the geohash of the product footprint approximate position
- **iiiiiiiii** is the Maritime Mobile Service Identity (MMSI) of the depicted ship
- **VVvv** is the products version number of the product. The version number allows to identify the exact algorithm
  implementation used to generate the product.
  
  
## Product content

A Sentinel-2 ship product consists of
- a readme file with summary information on its content
- a GeoTIFF encoded subset of data from a Sentinel-2 Level 2A product
- the ship track around the time of product captured
- an overview map showing the ships track against the footprint of the product
- an browse image showing a true-color image of the subset covering the ships position
- a list of pixels of the subset that are identified to cover the ship
- a checksum file listing the individual component checksums as reported by the tool **sha256sum**

### Sentinel-2 product subset

The subset is stored in a GeoTIFF file that contains the data surrounding a ship. While, in the original Sentinel-2 L2A product, the bands are stored in varying resolution. The subset product has all bands on a 10 m x 10 m raster. The resolution is increased by computing the additional points using a nearest neighbour algorithm. For each spectral band, the Sentinel-2 L2A data file with the highest resolution is selected and copied. As a result, the following source bands are used: 
B01_60m, B02_10m, B03_10m, B04_10m, B05_20m, B06_20m, B07_20m, B08_10m, B8A_20m, B09_60m, B11_20m, B12_20m. In addition the Scene classification data band SCL_20m is also copied. 

The Scene classification data is defined in the Sentinel-2 MSI L2A product specification. A copy is provided here for convenience:

<table>
  <tr><th>Value</th><th>Definition</th></tr>
  <tr><td>0</td><td>NO_DATA</td></tr>
  <tr><td>1</td><td>SATURATED_OR_DEFECTIVE</td></tr>
  <tr><td>2</td><td>DARK_AREA_PIXELS</td></tr>
  <tr><td>3</td><td>CLOUD_SHADOWS</td></tr>
  <tr><td>4</td><td>VEGETATION</td></tr>
  <tr><td>5</td><td>BARE_SOILS or NON_VEGETATED</td></tr>
  <tr><td>6</td><td>WATER</td></tr>
  <tr><td>7</td><td>UNCLASSIFIED</td></tr>
  <tr><td>8</td><td>CLOUD_MEDIUM_PROBABILITY</td></tr>
  <tr><td>9</td><td>CLOUD_HIGH_PROBABILITY</td></tr>
  <tr><td>10</td><td>THIN_CIRRUS</td></tr>
  <tr><td>11</td><td>SNOW /ICE</td></tr>
</table>

Coincidentally, the ship pixels are mostly classified as 6=WATER in the SCL data.

### Ship track

Ship track information is provided in the common [ship track format specification](../COMMON/ShipTrack.md).

### Pixel list

The pixel list file shows the coordinates of the pixels that have been identified as being ship or wake. The format is described in
the common [pixel list format specification](../COMMON/PixelList.md).

### metadata.json

The file metadata.json contains metadata on the product in json format.

<table>
  <tr><th>Item</th><th>Description</th></tr>
  <tr><td>detectedShip</td><td>Information on the detected ship as deduced from its AIS data.</td></tr>
  <tr><td>detectedShipPixels</td><td>Number of initially detected pixels. The pixel detection algorithm is very basic and only serves as an initial plausibility measure. The algorithm cannot distinguish between the ships body and potentially the wake. The actual number of ship affected pixel may be larger or much smaller than the given value.</td></tr>
  <tr><td>expectedShipPixels</td><td>The expected number of ship pixels based on the ships size.</td></tr>
  <tr><td>geohash</td><td>The geo hash of the ship position.</td></tr>
  <tr><td>nonWaterShipPixelCount</td><td>The number of pixels classified to be part of the ship that are not classified as Water in the SCL band.</td></tr>
  <tr><td>sceneClassificationStats</td><td>Statistics on the scene classification of the pixels of the subset. For information on the semantics of the classes, please check the source product type metadata or documentation. </td></tr>
  <tr><td>shipPixelDetector</td><td>Information on the algorithm used to perform the initial approximate ship detection.</td></tr>
  <tr><td>sourceName</td><td>The name of the original Sentinel-2 product that the subset in this product has been taken from.</td></tr>
</table>

## Processing notes

The product is created by matching AIS position data to Sentinel-2 L2A product tiles. When a ship is reported to be visible in the product, a basic detection algorithm is employed to verify that the ship is indeed at the expected location, is visible and easily discernible from its surrounding. 

Reasons for discarding a ship position may be:
- The course of the ship is irregular, so that the ship cannot be found by interpolation of the known and reported positions.
- The ship is under clouds.
- The ship is not discernible from its surrounding, e.g. because there are breaking waves or it is moored at a quay.

## Examples and how-tos

Sample products can be found in [sample](sample) folder.

![Samples](samples1.png "Samples")

### How to display a product in QGis

This is a very concise guide on how to display the product content in [QGIS](https://www.qgis.org). It uses the sample product found under [sample](sample):

1. Unzip the product package: `unzip PB_S22ASHS__20210117T100035_sq9bzbwv_247087700_0001.zip`
2. Open a project in QGIS
3. Add the product content file `S2A_MSIL2A_20210117T095341_N0214_R079_T33SVA_20210117T120803_subs_247087700.tif` as a raster layer to the project: "Ctrl+Shift+R / Select file"
4. Select layer and use "Zoom to layer" to show the product.
5. Possibly adjust the rendering of the layer (Properties / Symbology), e.g. to show the values of a specific band, choose "Singleband gray" and select the band you are interested in.

## Where to get data
Data packages can be ordered from [petabite.eu](https://petabite.eu/products/PB_S22A_SHS). 

---
* License of documentation: [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/)
* License of sample data: [ODC-BY 1.0](sample/LICENSE)
* Get in touch: [Imprint](https://petabite.eu/imprint)
