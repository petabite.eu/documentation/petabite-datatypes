# Sentinel-2 ship sighting product examples

This folder contains example data products in a version that is compatible with the toolset provided in tools. The data is provided as is without
any warranty and under the license terms provided in the LICENSE file.

For further documentation please see [main readme](../../README.md)

Contains modified Copernicus Sentinel data 2021.

---
* License of documentation: [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/)
* License of sample data: [ODC-BY 1.0](LICENSE)