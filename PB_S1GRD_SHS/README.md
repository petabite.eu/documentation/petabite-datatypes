![Ships](ship_topic.jpg "Ships")


# Sentinel-1 Ship Sighting Product

Description and tools for Sentinel-1 ship sighting products.

## Introduction

The Sentinel-1 ship sighting product contains bundled information on ships that have been captured by the Sentinel-1 sensors. With 10 m resolution, the Sentinel-1 data is sufficient to identify ships and even derive some approximate information on their size and orientation. Smaller boats can also be detected through their wake.

This ship sighting bundle product provides a 
- Sentinel-1 VV, VH band data around a ship position
- A pixel list of pixels with a high probability of representing a ship
- Ship position and track data

The product is intended to be used for the training of statistical and deep learning models for the detection of ships in Sentinel-1 data. A large number of products can be provided both from past Sentinel-1 data and from recent Sentinel-1 products as they are made available by Copernicus.

## Product and product component naming

The product is named according to the following pattern:

PB_S1GRD_SHS_YYYYmmDDTHHMMSS_gggggg_iiiiiiiii_VVvv.zip

where
- **PB_** is a prefix 
- **S1GRD_SHS** is the fixed typecode identifer
- **YYYYmmDDTHHMMSS** is the approximate time and date of the acquisition
- **gggggg** is the geohash of the product footprint approximate position
- **iiiiiiiii** is the Maritime Mobile Service Identity (MMSI) of the depicted ship
- **VVvv** is the products version number of the product. The version number allows to identify the exact algorithm
  implementation used to generate the product.
  
  
## Product content

A Sentinel-1 ship product consists of
- a readme file with summary information on its content
- a GeoTIFF encoded subset of data from a Sentinel-1 GRDH product
- the ship track around the time of product captured
- an overview map showing the ships track against the footprint of the product
- an browse image showing a true-color image of the subset covering the ships position
- a list of pixels of the subset that are identified to cover the ship
- a checksum file listing the individual component checksums as reported by the tool **sha256sum**

### Sentinel-1 product subset

The subset is stored in a GeoTIFF file that contains the data surrounding a ship. The subset product includes the bands 
Amplitude_VV, Amplitude_VH as computed by the ESA Snap Toolbox Terrain-Correction operator using "nearest-neighbor" mode.

### Ship track

Ship track information is provided in the common [ship track format specification](../COMMON/ShipTrack.md).

### Pixel list

The pixel list file shows the coordinates of the pixels that have been identified as being ship or wake. The format is described in
the common [pixel list format specification](../COMMON/PixelList.md).

### metadata.json

The file metadata.json contains metadata on the product in json format.

<table>
  <tr><th>Item</th><th>Description</th></tr>
  <tr><td>detectedShip</td><td>Information on the detected ship as deduced from its AIS data.</td></tr>
  <tr><td>detectedShipPixels</td><td>Number of initially detected pixels. The pixel detection algorithm is very basic and only serves as an initial plausibility measure. The algorithm cannot distinguish between the ships body and potentially the wake. The actual number of ship affected pixel may be larger or much smaller than the given value.</td></tr>
  <tr><td>expectedShipPixels</td><td>The expected number of ship pixels based on the ships size.</td></tr>
  <tr><td>geohash</td><td>The geo hash of the ship position.</td></tr>
  <tr><td>shipPixelDetector</td><td>Information on the algorithm used to perform the initial approximate ship detection.</td></tr>
  <tr><td>sourceName</td><td>The name of the original data product that the subset in this product has been taken from.</td></tr>
</table>

## Processing notes

The product is created by matching AIS position data to Sentinel-1 GRDH. When a ship is reported to be visible in the product, a basic detection algorithm is employed to verify that the ship is indeed at the expected location and easily discernible from its surrounding.

Reasons for discarding a ship position may be:
- The course of the ship is irregular, so that the ship cannot be found by interpolation of the known and reported positions.
- The ship is not discernible from its surrounding, e.g. because there are breaking waves, it is moored at a quay or there are several ships in 
  close proximity.

## Examples and how-tos

Sample products can be found in [sample](sample) folder.

### How to display a product in QGis

This is a very concise guide on how to display the product content in [QGIS](https://www.qgis.org). It uses the sample product found under [sample](sample):

1. Unzip the product package: `unzip PB_S1GRD_SHS_20210821T054539_snd1pr6b_636092958_0100.zip`
2. Open a project in QGIS
3. Add the product content file `S1B_IW_GRDH_1SDV_20210821T054539_20210821T054604_028340_0361A3_1B9F_subs_636092958.tif` as a raster layer to the project: "Ctrl+Shift+R / Select file"
4. Select layer and use "Zoom to layer" to show the product.
5. Possibly adjust the rendering of the layer (Properties / Symbology), e.g. to show the values of a specific band, choose "Singleband gray" and select the band you are interested in.

## Where to get data
Data packages can be ordered from [petabite.eu](https://petabite.eu/products/PB_S1GRD_SHS). 

---
* License of documentation: [CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0/)
* License of sample data: [ODC-BY 1.0](sample/LICENSE)
* Get in touch: [Imprint](https://petabite.eu/imprint)
