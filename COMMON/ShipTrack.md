![Petabite](../Logo_Petabite_small.jpg "Logo")

# Data types documentation

## Ship track
The ship track file (*_realm_*.csv) contains the ship track around the time of the acquisition. It contains data at least an hour of data from before the acquisition and and some data after. The pre-acquisition track is provided because the keel water of the ship may show for a significant of time and thus affect the acquisition. 

The csv file has the columns:

<table>
<tbody>
  <tr><th>Item</th><th>Description</th></tr>
  <tr><td>time</td><td>The time of the reading</td></tr>
  <tr><td>callsign</td><td>The international radio call sign of the ship.</td></tr>
  <tr><td>courseOverGroundInDegree</td><td>The direction of the ship track over ground relative to North.</td></tr>
  <tr><td>currentHeadingInDegree</td><td>The heading of the ship relative to North.</td></tr>
  <tr><td>imo</td><td>The IMO ship identification number.</td></tr>
  <tr><td>mmsi</td><td>The Maritime Mobile Service Identity. This is the only identifier that is guaranteed to be available in this dataset. imo or callsign may be empty.</td></tr>
  <tr><td>name</td><td>The name of the ship.</td></tr>
  <tr><td>navstat</td><td>The navigation status as a human readable text</td></tr>
  <tr><td>position</td><td>The position as a json string.</td></tr>
  <tr><td>rateOfTurnInDegreesPerMinute</td><td>The reported rate of turn. Negative values indicate a left, positive values a right turn.</td></tr>
  <tr><td>speedOverGroundInMeterPerSecond</td><td>The speed over ground.</td></tr>
  <tr><td>turnStatus</td><td>The ships turning status. A value of 0 signifies no turning, a negative value left turning and a positive value right turning.</td></tr>
  <tr><td>vesselType</td><td>The typecode of the vessel.</td></tr>
  <tr><td>geometry</td><td>The reported position as WKT.</td></tr>
</tbody>
</table>
