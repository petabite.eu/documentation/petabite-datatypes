![Petabite](../Logo_Petabite_small.jpg "Logo")

# Data types documentation

## Pixel list

The pixel list file shows the coordinates of the pixels that have been identified as being ship or wake.

<table >
<tbody >
  <tr><th>Item</th><th>Description</th></tr>
  <tr><td>components</td><td>The list of files in the package.</td></tr>
  <tr><td>X (original)</td><td>The X pixel coordinate in the original product.</td></tr>
  <tr><td>Y (original)</td><td>The Y pixel coordinate in the original product.</td></tr>
  <tr><td>X (subset)</td><td>The X pixel coordinate in the subset product.</td></tr>
  <tr><td>Y (subset)</td><td>The Y pixel coordinate in the subset product.</td></tr>
  <tr><td>Value</td><td>The classifier value at the pixel location. The value type is dependent on the pixel detection algorithm (this value is only useful for potential misclassification investigations by the creator).</td></tr>
</tbody>
</table>
