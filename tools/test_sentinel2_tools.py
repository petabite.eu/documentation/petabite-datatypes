# -
# Copyright (C) Petabite GmbH, 2020- - All Rights Reserved
# Proprietary and confidential.
# Unauthorized copying of this file, via any medium is strictly prohibited.
# -
'''
Created on 10.03.2021

@author: Bernard Pruin

MIT License

Copyright (c) 2021 Petabite GmbH

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''

import unittest
import subprocess
import pandas as pd
import os

TEST_DIR="../PB_S22ASHS/sample"
TEST_PRODUCT=TEST_DIR + "/PB_S22ASHS__20210117T100035_sq9bzbwv_247087700_0100.zip"

base_args=["python3","sentinel2_tools.py"]

class Test(unittest.TestCase):

    def testHelp(self):
        res=subprocess.run(base_args + ["-h"],capture_output=True,check=True)
        assert len(res.stdout) == 1177
        
    def testList(self):
        res=subprocess.run(base_args + ["list",TEST_PRODUCT],capture_output=True,check=True)
        assert len(res.stdout) == 1035
        assert len(res.stderr) == 0

    def testInfo(self):
        res=subprocess.run(base_args + ["info",TEST_PRODUCT],capture_output=True,check=True)
        #print(len(res.stdout))
        assert len(res.stdout) == 2557
        
    def testInventory(self):
        PKL_TARGET='/tmp/test_sentinel2_tools.pkl'
        res=subprocess.run(base_args + ["inventory",TEST_DIR,'--aspickle',PKL_TARGET],capture_output=True,check=True)
        df=pd.read_pickle(PKL_TARGET)
        assert df.iloc[0]['detectedShipPixels'] == 12
        os.remove(PKL_TARGET) 

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testHelp']
    unittest.main()