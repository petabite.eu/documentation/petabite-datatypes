#!/usr/bin/env python3
'''
Created on 10.03.2021

@author: Bernard Pruin

MIT License

Copyright (c) 2021 Petabite GmbH

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''

import argparse
import zipfile
import json
import os
import pprint
import rasterio
from rasterio.plot import show
import webbrowser
import regex as re
import pandas as pd
import sys

INFO_ACTION="info"
LIST_ACTION="list"
PLOT_ACTION="plot"
README_ACTION="readme"
INVENTORY_ACTION="inventory"

HELP_EPILOG="""
The available actions are
info - to show the metadata of the product
inventory - to create a csv list of the metadata of all the products in a directory
list - to show the files contained in the product package
plot - to plot a specific band of the product, the band is set using the --band option.
readme - extracts the readme.html file with images from the product to a local working directory and 
         tries to open a web browser on it.

Example:

sentinel2_tools.py readme ../example_product/PB_S22ASHS__20210117T100035_sq9bzbwv_247087700_0001.zip

"""

action_choices=[INFO_ACTION,LIST_ACTION,PLOT_ACTION,README_ACTION, INVENTORY_ACTION]

parser = argparse.ArgumentParser(description='Very basic tool to access Sentinel-2 ship sighting product data.',
                                 epilog=HELP_EPILOG,
                                 formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('action', type=str, help='The action to perform.', choices=action_choices)
parser.add_argument('input',  type=str, help='a path to a directory (for the inventory action) or to a product.')
parser.add_argument('--band', type=str, default="1", help='The band number. Used for the "plot" action.')
parser.add_argument('--aspickle', type=str, help='Write a pandas DataFrame pickle to the given filename')

# Dictionary of registered action commands
actions={}

def product_name_from_path(product_path:str):
    """Extracts the product name from the path argument"""
    return os.path.basename(product_path)[:-4]

def extract_metadata(product_path:str):
    """Retrieves the metadata dictionary from the product"""
    product_name=product_name_from_path(product_path)
    with zipfile.ZipFile(product_path) as myzip:
        mtd=json.loads(myzip.open(product_name + "/metadata.json").read())
    return mtd

## list content
def s2_list_content(product_path:str,**kwargs):
    """Lists the content of the product package"""
    with zipfile.ZipFile(product_path) as myzip:
        for name in myzip.namelist():
            print(name)

actions[LIST_ACTION]=s2_list_content

def s2_info(product_path:str,**kwargs):
    """Extracts and shows the metadata of a product package"""
    mtd = extract_metadata(product_path)
    pprint.pprint(mtd,sort_dicts=True)

actions[INFO_ACTION]=s2_info

def s2_plot(product_path:str,**kwargs):
    """Plots a selected band of the product"""
    product_name=product_name_from_path(product_path)
    tiff=product_name + '/' + extract_metadata(product_path)['components']['productContent']    
    with rasterio.open('zip://'+ product_path +'!' + tiff) as dataset:
        show(dataset.read(int(kwargs['band'])),title=product_name + "(Band " + kwargs['band'] + ")")

actions[PLOT_ACTION]=s2_plot

def s2_readme(product_path:str,**kwargs):
    """Extracts the readme, together with images of a product and tries to open it in a browser on the host."""
    product_name=product_name_from_path(product_path)
    mtd=extract_metadata(product_path)
    quicklook=mtd['components']['quicklook']
    overview=mtd['components']['overview']    
    readme=product_name + '/readme.html'
    with zipfile.ZipFile(product_path) as myzip:
        with open("readme.html","w") as target:
            target.write(myzip.open(readme).read().decode("utf-8"))
        with open(quicklook,"wb") as ql:
            ql.write(myzip.open(product_name + "/" + quicklook).read())
        with open(overview,"wb") as ov:
            ov.write(myzip.open(product_name + "/" + overview).read())
    webbrowser.open("file:readme.html")

actions[README_ACTION]=s2_readme

def s2_inventory(dirpath:str,**kwargs):
    """Extracts metadata from all products in a directory and creates a flat list"""
    invlist=[]
    indexlist=[]
    r = re.compile('PB_S22ASHS_.*.zip')
    for dirName, subdirList, fileList in os.walk(dirpath, topdown=False):
        for match in [line for line in fileList if r.match(line)]:
            try:
                indexlist.append(match)
                mtd = extract_metadata(dirName + "/" + match)
                # flatten the detectedShip structure
                mtd.update(mtd['detectedShip'])
                del mtd['detectedShip']
                # drop a number static components
                del mtd['creator']
                del mtd['copyright']
                del mtd['shipPixelDetector']
                # drop some details
                del mtd['sceneClassificationStats']
                del mtd['components']
                invlist.append(mtd)
            except Exception:
                print("Error handling file " + match, file=sys.stderr)
    df = pd.DataFrame(invlist,index=indexlist)
    if kwargs['aspickle'] is None:
        print(df.to_csv())
    else:
        df.to_pickle(kwargs['aspickle'])
        
actions[INVENTORY_ACTION]=s2_inventory

if __name__ == '__main__':
    args=parser.parse_args()
    # select an action function based on the command line action argument
    actions[args.action](args.input,band=args.band,aspickle=args.aspickle)